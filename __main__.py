import random
import pandas as pd
import numpy as np
from os import listdir, remove, path, makedirs
from os.path import isfile, join

ROOT_DIR = path.dirname(path.abspath(__file__))

must_continue = True
deck_dir = ROOT_DIR + r"/decks"
full_deck_path = ROOT_DIR + r"/Full.deck"
commands = ["clone", "decks", "draw", "exit", "help", "load", "new", "remove", "restore", "show", "throw"]
cmd_description = [
    "Create a deck from an existing one (ex: clone 1 test) (alias: cp)",
    "Show available decks (alias: ls)",
    "Draw a random card in the deck (ex: draw 10)",
    "Close the simulator and save current deck (aliases: quit, stop, end)",
    "Display help (alias: h)",
    "Load an existing deck in the application using the id displayed with decks command and save current"
    " deck (ex: load 1) (alias: cd)",
    "Create a new deck of 34 cards (ex: new test)",
    "Remove a deck using the id displayed with decks command (ex: remove 1)",
    "Restore all 34 cards of the current deck",
    "Show all remaining cards in the deck",
    "Remove definitely a random card from the deck and display its name, you can throw multiple card at"
    " a time (ex: throw 10)"
  ]

deck = pd.DataFrame()
n_deck = 0


def title():
    print("====================================")
    print("D&D Playing Cards Deck Simulator")
    print("====================================\n")


def show_decks():
    print("Available Decks:")
    for i, e in enumerate(available_decks):
        print(f"{i}: {e}")


def show_help():
    for cmd_desc in zip(commands, cmd_description):
        print(f"{cmd_desc[0]}: {cmd_desc[1]}")


def get_card(n_card):
    global deck
    card = deck.iloc[n_card]
    print(f"{card[0]}: {card[1]}")


def remove_card(n_card):
    global deck
    deck = pd.DataFrame(np.array(deck.drop([n_card])))


def throw(n_card):
    for i in range(n_card):
        card_id = random.randint(0, len(deck) - 1)
        get_card(card_id)
        remove_card(card_id)
    save()


def restore():
    global deck
    deck = pd.read_csv(full_deck_path, sep=";", header=None)


def show():
    global deck
    for i in range(len(deck)):
        get_card(i)


def load(deck_n):
    global n_deck, deck
    save()
    n_deck = deck_n
    if n_deck == 0:
        print(f"Loaded Full.deck")
        deck = pd.read_csv(full_deck_path, sep=";", header=None)
    else:
        print(f"Loaded {available_decks[n_deck]}")
        deck = pd.read_csv(deck_dir + "/" + available_decks[n_deck], sep=";", header=None)


def save():
    global deck, n_deck
    if n_deck != 0:
        deck.to_csv(deck_dir + "/" + available_decks[n_deck], sep=";", header=False, index=False)


def new_deck(name):
    global available_decks
    new_full_deck = pd.read_csv(full_deck_path, sep=";", header=None)
    new_full_deck.to_csv(deck_dir + "/" + name + ".deck", sep=";", header=False, index=False)
    available_decks = ["Full.deck"] + [f for f in listdir(deck_dir) if isfile(join(deck_dir, f))]


def remove_deck(deck_n):
    global available_decks
    if deck_n != 0:
        print(f"{available_decks[deck_n]} will be removed, type \"yes\" or \"y\" to confirm...")
        resp = input()
        if str.lower(resp) == "yes" or str.lower(resp) == "y":
            remove(deck_dir + "/" + available_decks[deck_n])
    else:
        print(f"Cannot delete Full.deck")
    available_decks = ["Full.deck"] + [f for f in listdir(deck_dir) if isfile(join(deck_dir, f))]
    load(0)


def clone(deck_n, name):
    global available_decks
    if deck_n != 0:
        new_full_deck = pd.read_csv(deck_dir + "/" + available_decks[deck_n], sep=";", header=None)
    else:
        new_full_deck = pd.read_csv(full_deck_path, sep=";", header=None)
    new_full_deck.to_csv(deck_dir + "/" + name + ".deck", sep=";", header=False, index=False)
    available_decks = ["Full.deck"] + [f for f in listdir(deck_dir) if isfile(join(deck_dir, f))]


def draw(n_card):
    for i in range(n_card):
        card_id = random.randint(0, len(deck) - 1)
        get_card(card_id)


def handler(command):
    global must_continue
    split_cmd = str.split(command, sep=" ")
    verb = str.lower(split_cmd[0])
    if verb == "draw":
        if len(split_cmd) == 1:
            draw(1)
        else:
            draw(int(split_cmd[1]))
    elif verb == "remove" or verb == "rm":
        remove_deck(int(split_cmd[1]))
    elif verb == "clone" or verb == "cp":
        clone(int(split_cmd[1]), split_cmd[2])
    elif verb == "new":
        new_deck(split_cmd[1])
    elif verb == "show" or verb == "cat":
        show()
    elif verb == "restore":
        restore()
    elif verb == "throw":
        if len(split_cmd) == 1:
            throw(1)
        else:
            throw(int(split_cmd[1]))
    elif verb == "load" or verb == "cd":
        load(int(split_cmd[1]))
    elif verb == "decks" or verb == "ls":
        show_decks()
    elif verb == "help" or verb == "h":
        show_help()
    elif verb == "quit" or verb == "exit" or verb == "stop" or verb == "end":
        save()
        must_continue = False
    else:
        print(f"Command unknown: {verb}")


if __name__ == '__main__':
    if not path.exists(deck_dir):
        makedirs(deck_dir)
    available_decks = ["Full.deck"] + [f for f in listdir(deck_dir) if isfile(join(deck_dir, f))]
    title()
    load(0)
    print("Type \"help\" to see available commands...")
    while must_continue:
        try:
            handler(input("$ "))
        except:
            print("Invalid command")
