# Quick start

## Install dependencies
You will need python 3.8 or greater. Run the `python -m pip install -r requirements.txt` in the terminal in order to 
install dependencies.

## Run the project
If you are in the root directory just run `python __main__.py`. You can also run `python -m <path_to_module>` to run 
the project from outside the root directory (the path must be absolute).

Once the card simulator is running you can type `help` in the terminal in order to view a list of available command.